from graph import Ctx, User
from .OneUserInt import OneUserIntAction


class BeeHomeAction(OneUserIntAction):
    def __init__(self, ctx: Ctx):
        super().__init__(ctx)

    def getOutputMax(self, u: User) -> int:
        return u.getHonor()

    def updateWithOutput(self, u: User, O: float):
        honor = u.chBH(O)
        u.chHonor(-honor)
        return honor

    def test():
        u = User()
        ctx = Ctx()
        act = BeeHomeAction(ctx)
        possibleK = act.getPossibleK(u)
        assert possibleK is None
        u.chHonor(4)
        possibleK = act.getPossibleK(u)
        assert possibleK[0] == 0
        assert possibleK[1] == 0.2 * 4 + 0.2 / 2
        assert act.act(possibleK[1]) == 4
        assert u.getHonor() == 0
