from typing import Callable, Tuple

from graph import Ctx, User
from .Base import Action


class TransactionAction(Action):
    def __init__(self, ctx):
        super().__init__(ctx)

    def getLast(self) -> Tuple[User, User, Callable[[float], float]]:
        return super().getLast()

    def getPossibleK(self, uI: User, uO: User, checkBank: bool = False) -> Tuple[float, float]:
        HI = uI.getH()
        if HI == 0:
            return (0, 0)
        # TODO Взять ставку банка + случайный
        kH = self.c().getKtrans(uO if checkBank else None)
        self.setLast((uI, uO, kH))
        withTaks = HI - kH(HI)
        return (0, withTaks / HI)

    def act(self, k):
        (uI, uO, kH) = self.getLast()
        H = uI.getH() * k
        taks = kH(H)
        uI.chH(-H-taks)
        res = uO.chH(H)
        self.c().getBank().chH(taks)
        return res

    def test():
        ctx = Ctx()
        tc = TransactionAction(ctx)
        u1 = User()
        u2 = User()
        H1 = 1000
        H2 = 200
        Hm = 300
        u1.chH(H1)
        u2.chH(H2)
        # Get 300 H
        tc.getPossibleK(u1, u2)
        assert Hm == tc.act(Hm / u1.getH())
        assert u1.getH() == H1 - Hm * 1.001
        assert u2.getH() == H2 + Hm
        # Get rest of H
        pos = tc.getPossibleK(u1, u2)
        tc.act(pos[1])
        assert u1.getH() == 0
        assert u2.getH() == H1 * 0.999 + H2
        # Check bank count
        assert ctx.getBank().getH() - 1000000 == H1 * 0.001
