from graph import Ctx, User
from .OneUserInt import OneUserIntAction


class BeeWorkAction(OneUserIntAction):
    def __init__(self, ctx: Ctx):
        super().__init__(ctx)

    def getOutputMax(self, u: User) -> int:
        B2BH = self.c().getB2BH()[0](u.getB())
        BH = u.getBH()
        return min(B2BH, BH)

    def updateWithOutput(self, u: User, O: float):
        BH = u.chWBH(O)
        B = self.c().getB2BH()[1](BH)
        u.chWB(B)
        return BH

    def test():
        u = User()
        ctx = Ctx()
        act = BeeWorkAction(ctx)
        # B = 0; BH = 0
        possibleK = act.getPossibleK(u)
        assert possibleK is None
        u.chB(12)
        # B = 12; BH = 0
        possibleK = act.getPossibleK(u)
        assert possibleK is None
        u.chBH(2)
        # B = 12; BH = 2
        possibleK = act.getPossibleK(u)
        assert possibleK[0] == 0
        assert possibleK[1] == 1/2 + 1/4
        u.chB(18)
        # B = 30; BH = 2
        possibleK = act.getPossibleK(u)
        assert possibleK[1] - 2/3 - 1/6 < 0.000001
        u.chBH(1)
        # B = 30; BH = 3
        possibleK = act.getPossibleK(u)
        assert possibleK[1] == 3/4 + 1/8
        u.chBH(4)
        # B = 30; BH = 7
        possibleK = act.getPossibleK(u)
        assert possibleK[1] == 3/4 + 1/8
        assert act.act(possibleK[1]) == 3
        assert u.getB() == 0
        assert u.getBH() == 7 - 3
        assert u.getWB() == 30
        assert u.getWBH() == 3
