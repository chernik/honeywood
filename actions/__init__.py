from .Transaction import TransactionAction
from .PvP import PvPAction
from .H2B import H2BAction
from .H2W import H2WAction
from .BeeHome import BeeHomeAction
from .BeeWork import BeeWorkAction


def test():
    TransactionAction.test()
    PvPAction.test()
    H2BAction.test()
    H2WAction.test()
    BeeHomeAction.test()
    BeeWorkAction.test()
