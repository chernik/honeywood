from typing import Tuple

from graph import Ctx, User
from .Base import Action
from .Transaction import TransactionAction


class P2PAction(Action):
    def __init__(self, ctx: Ctx):
        super().__init__(ctx)

    def getLast(self) -> Tuple[User, User, TransactionAction]:
        return super().getLast()

    def getAvailable(self, _uO: User):
        raise NotImplementedError('getAvailable')

    def moveProportial(self, _uI, _uO, _H):
        raise NotImplementedError('moveProportial')

    def getPossibleK(self, uI: User, uO: User):
        if uI.getH() == 0:
            return None
        transactionH = TransactionAction(self.c())
        transactionK = transactionH.getPossibleK(uI, uO, True)
        availableH = self.getAvailable(uO)
        availableK = (0, availableH / uI.getH())
        rangeK = (
            max(transactionK[0], availableK[0]),
            min(transactionK[1], availableK[1]),
        )
        self.setLast((uI, uO, transactionH))
        if rangeK[0] >= rangeK[1]:
            return None
        return rangeK

    def act(self, k: float):
        (uI, uO, transactionH) = self.getLast()
        givenH = k * uI.getH()
        actualH = self.moveProportial(uI, uO, givenH)
        res = transactionH.act(actualH / uI.getH())
        return res
