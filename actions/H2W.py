from .P2P import P2PAction

from graph import Ctx, User


class H2WAction(P2PAction):
    def __init__(self, ctx: Ctx):
        super().__init__(ctx)

    def getAvailable(self, uO: User):
        return self.c().getKH2W()[1](uO.getW())

    def moveProportial(self, uI: User, uO: User, H: float):
        (W, H4W) = self.c().getH2W()(H)
        uO.chW(-W)
        uI.chW(W)
        return H4W

    def test():
        # Get wood from bank
        ctx = Ctx()
        u1 = User()
        u2 = ctx.getBank()
        u1.chH(1000)
        act = H2WAction(ctx)
        # No wood
        possibleK = act.getPossibleK(u1, u2)
        assert possibleK is None
        # Buy 196 wood
        u2.chW(500)
        possibleK = act.getPossibleK(u1, u2)
        # -- We can use all money, so 100% - 10% = 90%
        assert possibleK[1] == 0.9
        assert act.act(0.199) == 196
        assert u1.getH() == 1000 - 196 * 1.1
        assert u1.getH() + u2.getH() == 1000000 + 1000
        assert u1.getW() == 49
        assert u2.getW() == 500 - 49
        assert u1.getHonor() == 1
        # Buy more 200 wood
        possibleK = act.getPossibleK(u1, u2)
        assert possibleK[1] == 0.9
        assert act.act(200 / u1.getH()) == 200
        assert u1.getH() == 1000 - 396 * 1.1
        assert u1.getH() + u2.getH() == 1000000 + 1000
        assert u1.getW() == 99
        assert u2.getW() == 500 - 99
        assert u1.getHonor() == 3
        # Use honor
        u1.chHonor(-2)
        assert u1.getHonor() == 1
        # Buy rest of wood
        possibleK = act.getPossibleK(u1, u2)
        act.act(possibleK[1])
        assert u1.getH() == 10
        assert u2.getH() == 1000000 + 1000 - 10
        assert u1.getW() == 225
        assert u2.getW() == 275
        assert u1.getHonor() + u2.getHonor() + 2 == 500 / 25
        # Check possibleK
        u1.chH(25 * 4 * 4 - u1.getH())
        u2.chW(25 - u2.getW())
        possibleK = act.getPossibleK(u1, u2)
        assert possibleK[0] == 0
        assert possibleK[1] == 0.25
