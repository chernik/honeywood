from graph import Ctx, User
from .Base import Action


class OneUserIntAction(Action):
    def __init__(self, ctx: Ctx):
        super().__init__(ctx)

    def getOutputMax(self, _u: User) -> int:
        raise NotImplementedError('getOutputMax')

    def updateWithOutput(self, _u: User, _O: float):
        raise NotImplementedError('updateWithOutput')

    def getPossibleK(self, u: User):
        O = self.getOutputMax(u)
        self.setLast((u,))
        if O == 0:
            return None
        return (0, 1 - 1 / (O + 1) / 2)

    def act(self, k):
        (u,) = self.getLast()
        O = self.getOutputMax(u)
        return self.updateWithOutput(u, (O + 1) * k)
