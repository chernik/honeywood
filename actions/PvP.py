from typing import Tuple

from graph import Ctx, User
from utils import rdH
from .Base import Action
from .Transaction import TransactionAction


class PvPActionInitiator(Action):
    def __init__(self, ctx):
        super().__init__(ctx)

    def getLast(self) -> Tuple[User, TransactionAction, float]:
        return super().getLast()

    # Доступные коэффициенты для игрока-инициатора
    def getPossibleK(self, uInitiator):
        if uInitiator.getH() == 0:
            return None
        transInitiator = TransactionAction(self.c())
        possibleInitiator = transInitiator.getPossibleK(
            uInitiator, self.c().getBank()
        )
        self.setLast((uInitiator, transInitiator))
        return possibleInitiator

    # Выбираем коэффициент для PvP
    def act(self, k):
        last = self.getLast()[:2]
        self.setLast(last + (k,))

    # Ставка
    def getH(self):
        last = self.getLast()
        self.setLast(last)
        (u, _, k) = last
        return k * u.getH()


class PvPAction(Action):
    def __init__(self, ctx):
        super().__init__(ctx)

    def getLast(self) -> Tuple[User, TransactionAction, PvPActionInitiator, float]:
        return super().getLast()

    # Смотрим, можем ли мы играть
    def getPossibleK(self, uSupplier: User, pvpInitiator: PvPActionInitiator):
        if uSupplier.getH() == 0:
            return None
        HInitiator = pvpInitiator.getH()
        transSupplier = TransactionAction(self.c())
        possibleSupplier = transSupplier.getPossibleK(uSupplier, self.c().getBank())
        actualK = HInitiator / uSupplier.getH()
        self.setLast((uSupplier, transSupplier, pvpInitiator, actualK))
        if possibleSupplier[0] <= actualK and actualK <= possibleSupplier[1]:
            return (actualK, actualK)
        return None

    # Играем
    def act(self):
        (uSupplier, transSupplier, pvpInitiator, kSupplier) = self.getLast()
        initiatorLast = pvpInitiator.getLast()
        pvpInitiator.setLast(initiatorLast)
        (uInitiator, transInitiator, kInitiator) = initiatorLast
        H1 = transInitiator.act(kInitiator)
        H2 = transSupplier.act(kSupplier)
        assert H1 == H2
        H = H1 + H2
        # Clear sum of bet
        self.c().getBank().chH(-H)
        H2W = self.c().getKwoodPvP()
        (W, H4W) = H2W(H)
        H -= H4W
        uWinner = self.c().getPvPWinner(uInitiator, uSupplier)
        uWinner.chW(W)
        return uWinner.chH(H)

    def test():
        # Init users, ctx & actions
        u1 = User()
        u2 = User()
        ctx = Ctx()
        actInit = PvPActionInitiator(ctx)
        act = PvPAction(ctx)
        H1 = 1000
        H2 = 200
        u1.chH(H1)
        u2.chH(H2)
        # Bet 300 H (overflow)
        Hbet = 300
        actInit.getPossibleK(u1)
        actInit.act(Hbet / u1.getH())
        possibleK = act.getPossibleK(u2, actInit)
        assert possibleK is None
        # Bet 150 H and u1 win
        Hbet = 150
        ctx.getPvPWinner = lambda u1, _u2: u1
        actInit.getPossibleK(u1)
        actInit.act(Hbet / u1.getH())
        possibleK = act.getPossibleK(u2, actInit)
        assert possibleK[0] == possibleK[1]
        assert possibleK[0] == Hbet / u2.getH()
        assert u1.getH() == H1
        assert u2.getH() == H2
        act.act()
        assert u1.getH() == H1 + Hbet * 0.999 - u1.getW() * 4
        assert u2.getH() == rdH(H2 - Hbet * 1.001)
        # Bet 150 H again and u2 win
        ctx.getPvPWinner = lambda _u1, u2: u2
        actInit.getPossibleK(u1)
        actInit.act(Hbet / u1.getH())
        possibleK = act.getPossibleK(u2, actInit)
        act.act()
        # -- Hbet taked twice
        assert u1.getH() + u1.getW() * 4 == H1 - Hbet * 2 * 0.001
        assert u2.getH() + u2.getW() * 4 == H2 - Hbet * 2 * 0.001
        assert u2.getW() == 7
        # -- Same wood count
        assert u1.getW() == u2.getW()
        # Check bank H
        # -- Hbet taked twice for each gamer
        assert rdH(ctx.getBank().getH() - 1000000) == Hbet * 2 * 2 * 0.001
