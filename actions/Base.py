from typing import Tuple
from graph import Ctx


class Action:
    def __init__(self, ctx: Ctx):
        self.__ctx = ctx
        self.__last = None

    def getPossibleK(self):
        # Provide as many args as you want for action
        # Do not produce this args to `act`
        # Use `setLast` for that
        raise NotImplementedError('getPossibleK')

    def act(self):
        # Produce args only with final desition
        # Use `getLast` for get context of action
        raise NotImplementedError('act')

    def setLast(self, last: Tuple):
        self.__last = last

    def getLast(self) -> Tuple:
        last = self.__last
        if last is None:
            raise Exception('No last here')
        self.__last = None
        return last

    def c(self):
        return self.__ctx
