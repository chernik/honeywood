from .P2P import P2PAction

from graph import Ctx, User
from utils import rdH



class H2BAction(P2PAction):
    def __init__(self, ctx: Ctx):
        super().__init__(ctx)

    def getAvailable(self, uO: User):
        return self.c().getKH2B()[1](uO.getB())

    def moveProportial(self, uI: User, uO: User, H: float):
        (B, H4B) = self.c().getH2B()(H)
        uO.chB(-B)
        uI.chB(B)
        return H4B

    def test():
        u1 = User()
        u2 = User()
        u1.chH(1000)
        ctx = Ctx()
        act = H2BAction(ctx)
        # No bees
        possibleK = act.getPossibleK(u1, u2)
        assert possibleK is None
        # 200 bees
        u2.chB(500)
        possibleK = act.getPossibleK(u1, u2)
        assert possibleK[1] == 0.5
        assert act.act(0.2) == 200
        assert u1.getH() == 1000 - 200 * 1.001
        assert u2.getH() == 200
        assert u1.getB() == 200
        assert u2.getB() == 300
        # Rest of bees
        possibleK = act.getPossibleK(u1, u2)
        act.act(possibleK[1])
        assert u1.getH() == rdH(1000 - 500 * 1.001)
        assert u2.getH() == 500
        assert u1.getB() == 500
        assert u2.getB() == 0
