import random
from typing import List
import matplotlib.pyplot as plt
from actions.BeeHome import BeeHomeAction
from actions.BeeWork import BeeWorkAction

from graph import User, Ctx
from actions.PvP import PvPActionInitiator, PvPAction
from actions.H2B import H2BAction
from utils import rdW

grph = ([], [])

ctx = Ctx()
PvPs = []
users: List[User] = []

def r(v):
    return random.random() < v

def r2(psb):
    x = random.random()
    y = ((x*2-1)**7+1)/4
    return psb[0] + y * (psb[1] - psb[0])

def rn(arr):
    return arr[random.randint(0, len(arr) - 1)]

def inp(psb):
    return psb is None or psb[0] > psb[1]

def PvPInitiator(u: User):
    for asd in PvPs:
        if asd[0] is u:
            return
    if not r(0.2):
        return
    act = PvPActionInitiator(ctx)
    psb = act.getPossibleK(u)
    if inp(psb):
        return
    v = r2(psb)
    act.act(v)
    PvPs.append((u, act))

def PvPSupplier(u: User):
    if not r(0.2):
        return
    act = PvPAction(ctx)
    res = []
    for asd in PvPs:
        if asd[0] is u:
            continue
        actI = asd[1]
        psb = act.getPossibleK(u, actI)
        if inp(psb):
            continue
        res.append((asd, psb[0]))
    if len(res) == 0:
        return
    resSorted = sorted(res, key=lambda d: d[1])
    asd = rn(resSorted)[0]
    actI = asd[1]
    psb = act.getPossibleK(u, actI)
    act.act()
    PvPs.remove(asd)

def BeeHome(u: User):
    if not r(1):
        return
    act = BeeHomeAction(ctx)
    psb = act.getPossibleK(u)
    if inp(psb):
        return
    v = r2(psb)
    act.act(v)

def H2B(u: User):
    if not r(0.4):
        return
    res = []
    for u2 in users:
        if u is u2:
            continue
        act = H2BAction(ctx)
        psb = act.getPossibleK(u, u2)
        if inp(psb):
            continue
        res.append(u2)
    res.append(ctx.getBank())
    u2 = rn(res)
    act = H2BAction(ctx)
    psb = act.getPossibleK(u, u2)
    if inp(psb):
        return
    v = r2(psb)
    act.act(v)

def BeeWork(u: User):
    if not r(1):
        return
    act = BeeWorkAction(ctx)
    psb = act.getPossibleK(u)
    if inp(psb):
        return
    v = r2(psb)
    act.act(v)

def main():
    for i in range(10):
        u = User()
        u.chH(1000)
        users.append(u)
    try:
        for i in range(100000):
            if (i % 100) == 0:
                u = User()
                u.chH(1000)
                users.append(u)
            for u in users:
                if not r(0.1):
                    continue
                if r(0.01):
                    u.chH(100)
                BeeWork(u)
                BeeHome(u)
                PvPInitiator(u)
                PvPSupplier(u)
                H2B(u)
            if (i % 100) == 0:
                sW = 0
                for u in users:
                    sW += u.getW()
                k = sW / (len(users) + i / 5) / 100
                for u in users:
                    print(u.getLogStr())
                print('BANK:', ctx.getBank().getLogStr())
                print('k =', k)
                grph[0].append(i / 20)
                grph[1].append(sW)
                if k > 1:
                    k = 1
                for u in users:
                    B = u.getWB()
                    nH = rdW(B * k)
                    u.chH(nH)
        raise Exception()
    except:
        plt.plot(grph[0], grph[1])
        plt.show()

main()
