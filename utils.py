import math


def sgn(v):
    return -1 if v < 0 else 1


def rdH(h) -> float:
    return sgn(h) * round(abs(h) * 100) / 100


def rdW(w) -> int:
    return sgn(w) * math.floor(abs(w))


def test():
    # rdH
    assert rdH(30) == 30
    assert rdH(30.02) == 30.02
    assert rdH(30.024) == 30.02
    assert rdH(30.026) == 30.03
    assert rdH(-30) == -30
    assert rdH(-30.02) == -30.02
    assert rdH(-30.024) == -30.02
    assert rdH(-30.026) == -30.03
    # rdW
    assert rdW(30) == 30
    assert rdW(30.6) == 30
    assert rdW(-30) == -30
    assert rdW(-30.6) == -30


test()
