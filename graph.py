from typing import Tuple
import random

from utils import rdH, rdW


class User:
    def __init__(self):
        self.__H = 0
        self.__W = 0
        self.__B = 0
        self.__BH = 0
        self.__WB = 0
        self.__WBH = 0
        self.__overHonor = 0

    def getH(self) -> float: return self.__H
    def getW(self) -> int: return self.__W
    def getB(self) -> int: return self.__B
    def getBH(self) -> int: return self.__BH
    def getWB(self) -> int: return self.__WB
    def getWBH(self) -> int: return self.__WBH

    def getHonor(self) -> int:
        return rdW(self.getW() / 25) + self.__overHonor

    def chH(self, H):
        Hr = rdH(H)
        # 200.0 - 150.15 is 49.84(9)
        self.__H = rdH(self.__H + Hr)
        return Hr

    def chW(self, W):
        Hw = rdW(W)
        self.__W += Hw
        return Hw

    def chB(self, B):
        # TODO: for now as W
        Hb = rdW(B)
        self.__B += Hb
        return Hb

    def chBH(self, BH):
        iBH = rdW(BH)
        self.__BH += iBH
        return iBH

    def chWB(self, WB):
        iWB = rdW(WB)
        self.chB(-iWB)
        self.__WB += iWB
        return iWB

    def chWBH(self, WBH):
        iWBH = rdW(WBH)
        self.chBH(-iWBH)
        self.__WBH += iWBH
        return iWBH

    def chHonor(self, honor):
        h = rdW(honor)
        self.__overHonor += h
        return h

    def getLogStr(self):
        return 'H = %10.2f | W = %8d | B = %10s | BH = %10s | h = %5d' % (
            self.getH(),
            self.getW(),
            f'{self.getB()}/{self.getWB()}',
            f'{self.getBH()}/{self.getWBH()}',
            self.getHonor(),
        )


class Ctx:
    def __init__(self):
        self.__bank = User()
        self.__bank.chH(1000000)
        self.__bank.chB(1000000)

    def getBank(self):
        return self.__bank

    def getKtrans(self, uO):
        k = 0.1 if uO is self.getBank() else 0.001
        return lambda H: H * k

    def getKH2W(self):
        return (
            lambda H: rdW(H * 0.25),
            lambda W: rdW(W * 4)
        )

    def getKH2B(self):
        return (
            lambda H: rdW(H),
            lambda W: rdW(W)
        )

    def getB2BH(self):
        return (
            lambda B: rdW(B * 0.1),
            lambda BH: rdW(BH * 10)
        )

    def __getH2Ol(self, H, H2O, O2H):
        O = H2O(H)
        H4O = O2H(O)
        return (O, H4O)

    def __getH2O(self, gH2O):
        (H2O, O2H) = gH2O()
        return lambda H: self.__getH2Ol(H, H2O, O2H)

    def getH2W(self): return self.__getH2O(self.getKH2W)
    def getH2B(self): return self.__getH2O(self.getKH2B)

    def __getKwoodPvP(self, H) -> Tuple[int, float]:
        return self.getH2W()(H * 0.1)


    def getKwoodPvP(self):
        return self.__getKwoodPvP

    def getPvPWinner(self, u1: User, u2: User):
        if random.randint(0, 1) == 0:
            return u1
        return u2
